//
//  ViewController.swift
//  SideMenuExample
//
//  Created by Vigneshprabu on 13/09/19.
//  Copyright © 2019 Vigneshprabu. All rights reserved.
//

import UIKit

import SideMenuSwift

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func MenuClicked(_ sender: Any) {
        self.sideMenuController?.revealMenu()
    }
    
}

